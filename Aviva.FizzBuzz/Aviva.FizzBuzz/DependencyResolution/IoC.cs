using FizzBuzz.Business;
using StructureMap;
namespace Aviva.FizzBuzz {
    public static class IoC {
        public static IContainer Initialize() {
            ObjectFactory.Initialize(x =>
                        {
                            x.For<IFizzBuzzManagerLogic>().Use<FizzBuzzManagerLogic>();
                            x.For<IFizzBuzzManager>().Use<FizzManager>();
                            x.For<IFizzBuzzManager>().Use<BuzzManager>();
                            x.For<IFizzBuzzManager>().Use<FizzNBuzzManager>();
                            x.For<IDayProvider>().Use<DayProvider>();
                        });
            return ObjectFactory.Container;
        }
    }
}