﻿namespace FizzBuzz.Web.Controllers
{
    using System.Web.Mvc;
    using Business;
    using FizzBuzz.Web.Models;
    using System.Linq;
    using System.Collections.Generic;
    using FizzBuzz.Business.Resources;

    public class FizzBuzzController : Controller
    {
        private readonly IFizzBuzzManagerLogic fizzBuzzManagerLogic;
        private const int ItemsPerPage = 20;

        public FizzBuzzController(IFizzBuzzManagerLogic fizzBuzzManagerLogic)
        {
            this.fizzBuzzManagerLogic = fizzBuzzManagerLogic;
        }

        [HttpGet]
        public ActionResult FizzBuzz()
        {
            return View("FizzBuzzView");
        }

        [HttpPost]
        public ActionResult FizzBuzz(FizzBuzzModel fizzBuzzModel)
        {

            var fizzBuzzList = this.fizzBuzzManagerLogic.GetFizzBuzzList(fizzBuzzModel.InputValue);
            fizzBuzzModel.FizzBuzzValuesList = GetDisplayValues(fizzBuzzList, 1, ItemsPerPage);
            fizzBuzzModel.PageNumber = 1;
            fizzBuzzModel.ShowNextLink = IsNextLinkToBeDisplayed(fizzBuzzModel, fizzBuzzList);
            return View("FizzBuzzView", fizzBuzzModel);
        }

        private static bool IsNextLinkToBeDisplayed(FizzBuzzModel fizzBuzzModel, IReadOnlyCollection<string> fizzBuzzList)
        {
            var isNextLinkDisplayed = false;
            if (fizzBuzzList != null)
            {
                isNextLinkDisplayed = fizzBuzzModel.PageNumber <
                       ((fizzBuzzList.Count / ItemsPerPage) +
                        (fizzBuzzList.Count % ItemsPerPage == 0 ? 0 : 1));
            }
            return isNextLinkDisplayed;
        }

        public ActionResult DisplayPaging(int inputValue, int pageNumber, string linkText)
        {
            var fizzBuzzModel = new FizzBuzzModel
            {
                InputValue = inputValue,
                PageNumber = pageNumber
            };
            var fizzBuzzList = this.fizzBuzzManagerLogic.GetFizzBuzzList(fizzBuzzModel.InputValue);

            // Increment or Decrement Page Index
            if (linkText == FizzBuzzResource.Next)
                fizzBuzzModel.PageNumber = pageNumber + 1;
            else if (linkText == FizzBuzzResource.Prev)
                fizzBuzzModel.PageNumber = pageNumber - 1;

            fizzBuzzModel.ShowPrevLink = fizzBuzzModel.PageNumber > 1;
            fizzBuzzModel.ShowNextLink = IsNextLinkToBeDisplayed(fizzBuzzModel, fizzBuzzList);
            fizzBuzzModel.FizzBuzzValuesList = GetDisplayValues(fizzBuzzList, fizzBuzzModel.PageNumber, ItemsPerPage);

            return View("FizzBuzzView", fizzBuzzModel);
        }

        private static IEnumerable<string> GetDisplayValues(IEnumerable<string> fizzBuzzList, int pageNumber, int itemsShownPerPage)
        {
            return fizzBuzzList == null ? Enumerable.Empty<string>() : fizzBuzzList.Skip((pageNumber - 1) * itemsShownPerPage).Take(itemsShownPerPage).ToList();
        }
    }
}
