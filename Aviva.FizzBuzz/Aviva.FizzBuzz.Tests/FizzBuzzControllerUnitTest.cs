﻿namespace FizzBuzz.Web.Tests
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Business;
    using FluentAssertions;
    using Controllers;
    using Models;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using NUnit.Framework;
    using System.Linq;
    using System.Globalization;

    [TestClass]
    public class FizzBuzzControllerUnitTest
    {
        private Mock<IFizzBuzzManagerLogic> mockFizzBuzzManagerLogic;
        private FizzBuzzController controller;
        private const int ItemsPerPage = 20;

        [SetUp]
        public void TestInitialize()
        {
            this.mockFizzBuzzManagerLogic = new Mock<IFizzBuzzManagerLogic>();
            this.controller = new FizzBuzzController(this.mockFizzBuzzManagerLogic.Object);
        }

        [TestCase("FizzBuzzView")]
        public void FizzBuzzReturnsVallidView(string expectedViewName)
        {
            var fizzbuzzController = new FizzBuzzController(null);
            var actualResult = fizzbuzzController.FizzBuzz() as ViewResult;

            actualResult.Should().NotBeNull("Should return an ActionResult");
            if (actualResult != null) actualResult.ViewName.ShouldBeEquivalentTo(expectedViewName);
        }

        [TestCase(20)]
        public void FizzBuzzReturnsValidFizzBuzzSequenceForGivenNumber(int inputFizzBuzzNumber)
        {
            this.mockFizzBuzzManagerLogic.Setup(m => m.GetFizzBuzzList(inputFizzBuzzNumber)).Returns(GetFizzBuzzList(inputFizzBuzzNumber));

            var actualResult = this.controller.FizzBuzz(new FizzBuzzModel { InputValue = inputFizzBuzzNumber }) as ViewResult;

            actualResult.Should().NotBeNull("Should return an ActionResult");
            if (actualResult == null) return;
            var viewModel = (FizzBuzzModel)actualResult.Model;
            var resultsCount = viewModel.FizzBuzzValuesList.Count();
            resultsCount.ShouldBeEquivalentTo(inputFizzBuzzNumber);
            viewModel.FizzBuzzValuesList.ElementAt(0).ShouldBeEquivalentTo("1");
            viewModel.FizzBuzzValuesList.ElementAt(19).ShouldBeEquivalentTo("20");
        }


        private static List<string> GetFizzBuzzList(int rowsToBeTested)
        {
            var fizzbuzzlist = Enumerable.Range(1, rowsToBeTested).Select(i => i.ToString(CultureInfo.InvariantCulture)).ToList();
            return fizzbuzzlist;
        }

        [TestCase(21)]
        public void DisplayPagingReturnsNextLinkForGivenNumber(int inputFizzBuzzNumber)
        {
            this.mockFizzBuzzManagerLogic.Setup(m => m.GetFizzBuzzList(inputFizzBuzzNumber)).Returns(GetFizzBuzzList(inputFizzBuzzNumber));

            var actualResult = this.controller.DisplayPaging(inputFizzBuzzNumber, 0, "Next") as ViewResult;

            actualResult.Should().NotBeNull("Should return an ActionResult");
            if (actualResult == null) return;
            var viewModel = (FizzBuzzModel)actualResult.Model;
            var resultsCount = viewModel.FizzBuzzValuesList.Count();
            resultsCount.ShouldBeEquivalentTo(ItemsPerPage);
            viewModel.ShowNextLink.ShouldBeEquivalentTo(true);
            viewModel.ShowPrevLink.ShouldBeEquivalentTo(false);
        }

        [TestCase(65)]
        public void DisplayPagingReturnsNextAndPreviousLinkForGivenNumber(int inputFizzBuzzNumber)
        {
            this.mockFizzBuzzManagerLogic.Setup(m => m.GetFizzBuzzList(inputFizzBuzzNumber)).Returns(GetFizzBuzzList(inputFizzBuzzNumber));

            var actualResult = this.controller.DisplayPaging(inputFizzBuzzNumber, 1, "Next") as ViewResult;

            actualResult.Should().NotBeNull("Should return an ActionResult");
            if (actualResult == null) return;
            var viewModel = (FizzBuzzModel)actualResult.Model;
            var resultsCount = viewModel.FizzBuzzValuesList.Count();
            resultsCount.ShouldBeEquivalentTo(ItemsPerPage);
            viewModel.ShowNextLink.ShouldBeEquivalentTo(true);
            viewModel.ShowPrevLink.ShouldBeEquivalentTo(true);
        }

        [TestCase(25)]
        public void DisplayPagingReturnsPreviousLinkForGivenNumber(int inputFizzBuzzNumber)
        {
            this.mockFizzBuzzManagerLogic.Setup(m => m.GetFizzBuzzList(inputFizzBuzzNumber)).Returns(GetFizzBuzzList(inputFizzBuzzNumber));

            var actualResult = this.controller.DisplayPaging(inputFizzBuzzNumber, 1, "Next") as ViewResult;

            actualResult.Should().NotBeNull("Should return an ActionResult");
            if (actualResult == null) return;
            var viewModel = (FizzBuzzModel)actualResult.Model;
            var resultsCount = viewModel.FizzBuzzValuesList.Count();
            resultsCount.ShouldBeEquivalentTo(5);
            viewModel.ShowNextLink.ShouldBeEquivalentTo(false);
            viewModel.ShowPrevLink.ShouldBeEquivalentTo(true);
        }
    }
}
