﻿namespace FizzBuzz.Business.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NUnit.Framework;
    using FluentAssertions;
    using FizzBuzz.Business.Resources;

    [TestClass]
    public class BuzzManagerUnitTest
    {
        private BuzzManager buzzManager;

        [SetUp]
        public void TestInitialize()
        {
            this.buzzManager = new BuzzManager();
        }

        [TestCase(10, true)]
        [TestCase(8, false)]
        public void DivisibilityCheckForFiveReturnsTrueOrFalse(int inputFizzBuzzNumber, bool expectedResult)
        {
            var actualResult = this.buzzManager.IsNumberDivisible(inputFizzBuzzNumber);

            actualResult.ShouldBeEquivalentTo(expectedResult);
        }

        [TestCase(false)]
        [TestCase(true)]
        public void IsTodayWednesdayCheckReturnsTrueOrFalse(bool isTodayWednesday)
        {
            var expectedValue = isTodayWednesday ? FizzBuzzResource.Wuzz : FizzBuzzResource.Buzz;

            var actualResult = this.buzzManager.GetDisplayMessage(isTodayWednesday);

            actualResult.ShouldBeEquivalentTo(expectedValue);
        }

    }
}
