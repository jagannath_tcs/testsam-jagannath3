﻿namespace FizzBuzz.Business.Tests
{
    using FluentAssertions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NUnit.Framework;
    using Moq;
    using System.Collections.Generic;
    using FizzBuzz.Business.Resources;

    [TestClass]
    public class FizzBuzzManagerLogicUnitTest
    {
        private Mock<IDayProvider> dayProvider;
        private IEnumerable<IFizzBuzzManager> fizzBuzzManager;

        [SetUp]
        public void TestInitialize()
        {
            this.dayProvider = new Mock<IDayProvider>();
            this.fizzBuzzManager = new List<IFizzBuzzManager>()
            {
                MockFizzLogic().Object,
                MockBuzzLogic().Object,
                MockFizzBuzzLogic().Object
            };
        }

        [TestCase(15)]
        public void GetFizzBuzzListReturnsSequenceOfNumberWithFizzBuzz(int inputFizzBuzzNumber)
        {
            this.dayProvider.Setup(x => x.IsTodayWednesday()).Returns(false);
            var fizzbuzzLogic = new FizzBuzzManagerLogic(this.fizzBuzzManager, this.dayProvider.Object);

            var resultList = fizzbuzzLogic.GetFizzBuzzList(inputFizzBuzzNumber);

            resultList.Should().NotBeNullOrEmpty();
            resultList.Count.Should().Be(inputFizzBuzzNumber);
            resultList[2].Should().Be(FizzBuzzResource.Fizz);
            resultList[4].Should().Be(FizzBuzzResource.Buzz);
            resultList[14].Should().Be(FizzBuzzResource.FizzBuzz);
        }


        [TestCase(30)]
        public void GetFizzBuzzListReturnsSequenceOfNumberWithWizzWuzz(int inputFizzBuzzNumber)
        {
            this.dayProvider.Setup(x => x.IsTodayWednesday()).Returns(true);
            var fizzbuzzLogic = new FizzBuzzManagerLogic(this.fizzBuzzManager, this.dayProvider.Object);

            var resultList = fizzbuzzLogic.GetFizzBuzzList(inputFizzBuzzNumber);

            resultList.Should().NotBeNullOrEmpty();
            resultList.Count.Should().Be(inputFizzBuzzNumber);
            resultList[2].Should().Be(FizzBuzzResource.Wizz);
            resultList[4].Should().Be(FizzBuzzResource.Wuzz);
            resultList[14].Should().Be(FizzBuzzResource.WizzWuzz);
        }

        private static Mock<IFizzBuzzManager> MockFizzLogic()
        {
            var fizzManager = new Mock<IFizzBuzzManager>();
            fizzManager.Setup(x => x.Order).Returns(3);
            fizzManager.Setup(x => x.IsNumberDivisible(It.IsAny<int>())).Returns(false);
            fizzManager.Setup(x => x.IsNumberDivisible(3)).Returns(true);
            fizzManager.Setup(x => x.IsNumberDivisible(6)).Returns(true);
            fizzManager.Setup(x => x.IsNumberDivisible(9)).Returns(true);
            fizzManager.Setup(x => x.IsNumberDivisible(12)).Returns(true);
            fizzManager.Setup(x => x.IsNumberDivisible(15)).Returns(true);
            fizzManager.Setup(x => x.GetDisplayMessage(false)).Returns(FizzBuzzResource.Fizz);
            fizzManager.Setup(x => x.GetDisplayMessage(true)).Returns(FizzBuzzResource.Wizz);
            return fizzManager;
        }

        private static Mock<IFizzBuzzManager> MockBuzzLogic()
        {
            var buzzManager = new Mock<IFizzBuzzManager>();
            buzzManager.Setup(x => x.Order).Returns(2);
            buzzManager.Setup(x => x.IsNumberDivisible(It.IsAny<int>())).Returns(false);
            buzzManager.Setup(x => x.IsNumberDivisible(5)).Returns(true);
            buzzManager.Setup(x => x.IsNumberDivisible(10)).Returns(true);
            buzzManager.Setup(x => x.IsNumberDivisible(15)).Returns(true);
            buzzManager.Setup(x => x.GetDisplayMessage(false)).Returns(FizzBuzzResource.Buzz);
            buzzManager.Setup(x => x.GetDisplayMessage(true)).Returns(FizzBuzzResource.Wuzz);
            return buzzManager;
        }

        private static Mock<IFizzBuzzManager> MockFizzBuzzLogic()
        {
            var fizzBuzzManager = new Mock<IFizzBuzzManager>();
            fizzBuzzManager.Setup(x => x.Order).Returns(1);
            fizzBuzzManager.Setup(x => x.IsNumberDivisible(It.IsAny<int>())).Returns(false);
            fizzBuzzManager.Setup(x => x.IsNumberDivisible(15)).Returns(true);
            fizzBuzzManager.Setup(x => x.IsNumberDivisible(30)).Returns(true);
            fizzBuzzManager.Setup(x => x.GetDisplayMessage(false)).Returns(FizzBuzzResource.FizzBuzz);
            fizzBuzzManager.Setup(x => x.GetDisplayMessage(true)).Returns(FizzBuzzResource.WizzWuzz);
            return fizzBuzzManager;
        }

    }
}
