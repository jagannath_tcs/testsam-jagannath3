﻿namespace FizzBuzz.Business.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NUnit.Framework;
    using FluentAssertions;
    using FizzBuzz.Business.Resources;

    [TestClass]
    public class FizzBuzzManagerUnitTest
    {
        private FizzNBuzzManager fizzBuzzManager;

        [SetUp]
        public void TestInitialize()
        {
            this.fizzBuzzManager = new FizzNBuzzManager();
        }

        [TestCase(15, true)]
        [TestCase(14, false)]
        public void DivisibilityCheckForThreeAndFiveReturnsTrueOrFalse(int inputFizzBuzzNumber, bool expectedResult)
        {
            var actualResult = this.fizzBuzzManager.IsNumberDivisible(inputFizzBuzzNumber);

            actualResult.ShouldBeEquivalentTo(expectedResult);
        }

        [TestCase(false)]
        [TestCase(true)]
        public void IsTodayWednesdayCheckReturnsTrueOrFalse(bool isTodayWednesday)
        {
            var expectedValue = isTodayWednesday ? FizzBuzzResource.WizzWuzz : FizzBuzzResource.FizzBuzz;

            var actualResult = this.fizzBuzzManager.GetDisplayMessage(isTodayWednesday);

            actualResult.ShouldBeEquivalentTo(expectedValue);
        }
    }
}
