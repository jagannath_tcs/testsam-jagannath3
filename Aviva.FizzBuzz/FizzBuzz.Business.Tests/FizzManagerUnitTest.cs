﻿namespace FizzBuzz.Business.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NUnit.Framework;
    using FluentAssertions;
    using FizzBuzz.Business.Resources;

    [TestClass]
    public class FizzManagerUnitTest
    {
        private FizzManager fizzManager;

        [SetUp]
        public void TestInitialize()
        {
            this.fizzManager = new FizzManager();
        }

        [TestCase(5, false)]
        [TestCase(9, true)]
        public void DivisibilityCheckForThreeReturnsTrueOrFalse(int inputFizzBuzzNumber, bool expectedResult)
        {
            var actualResult = this.fizzManager.IsNumberDivisible(inputFizzBuzzNumber);

            actualResult.ShouldBeEquivalentTo(expectedResult);
        }

        [TestCase(false)]
        [TestCase(true)]
        public void IsTodayWednesdayCheckReturnsTrueOrFalse(bool isTodayWednesday)
        {
            var expectedValue = isTodayWednesday ? FizzBuzzResource.Wizz : FizzBuzzResource.Fizz;

            var actualResult = this.fizzManager.GetDisplayMessage(isTodayWednesday);

            actualResult.ShouldBeEquivalentTo(expectedValue);
        }
    }
}
