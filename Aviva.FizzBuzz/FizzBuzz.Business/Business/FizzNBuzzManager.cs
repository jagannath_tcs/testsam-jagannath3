﻿namespace FizzBuzz.Business
{
    using FizzBuzz.Business.Resources;

    public class FizzNBuzzManager : IFizzBuzzManager
    {
        public bool IsNumberDivisible(int inputFizzBuzzNumber)
        {
            return (inputFizzBuzzNumber % 15) == 0;
        }

        public string GetDisplayMessage(bool isTodayWednesday)
        {
            return isTodayWednesday ? FizzBuzzResource.WizzWuzz : FizzBuzzResource.FizzBuzz;
        }

        public int Order { get { return 1; } }
    }
}
