﻿namespace FizzBuzz.Business
{
    using FizzBuzz.Business.Resources;

    public class FizzManager : IFizzBuzzManager
    {
        public bool IsNumberDivisible(int inputFizzBuzzNumber)
        {
            return inputFizzBuzzNumber % 3 == 0;
        }

        public string GetDisplayMessage(bool isTodayWednesday)
        {
            return isTodayWednesday ? FizzBuzzResource.Wizz : FizzBuzzResource.Fizz;
        }

        public int Order { get { return 2; } }
    }
}
