﻿namespace FizzBuzz.Business
{
    using FizzBuzz.Business.Resources;

    public class BuzzManager : IFizzBuzzManager
    {
        public bool IsNumberDivisible(int inputFizzBuzzNumber)
        {
            return inputFizzBuzzNumber % 5 == 0;
        }

        public string GetDisplayMessage(bool isTodayWednesday)
        {
            return (isTodayWednesday ? FizzBuzzResource.Wuzz : FizzBuzzResource.Buzz);
        }

        public int Order { get { return 3; } }
    }
}
