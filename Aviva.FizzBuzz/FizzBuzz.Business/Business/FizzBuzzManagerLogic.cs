﻿
namespace FizzBuzz.Business
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Globalization;

    public class FizzBuzzManagerLogic : IFizzBuzzManagerLogic
    {
        private readonly IEnumerable<IFizzBuzzManager> fizzBuzzManager;
        private readonly IDayProvider dayProvider;

        public FizzBuzzManagerLogic(IEnumerable<IFizzBuzzManager> fizzBuzzManager, IDayProvider dayProvider)
        {
            this.fizzBuzzManager = fizzBuzzManager.OrderBy(x => x.Order).ToList();
            this.dayProvider = dayProvider;
        }

        public List<string> GetFizzBuzzList(int inputValue)
        {
            var fizzbuzzResultList = new List<string>();
            for (var counter = 1; counter <= inputValue; counter++)
            {
                var divionPolicies = this.fizzBuzzManager.Where(x => x.IsNumberDivisible(counter));
                var fizzBuzzItem = divionPolicies.FirstOrDefault();
                fizzbuzzResultList.Add(fizzBuzzItem != null
                    ? fizzBuzzItem.GetDisplayMessage(this.dayProvider.IsTodayWednesday())
                    : counter.ToString(CultureInfo.InvariantCulture));
            }
            return fizzbuzzResultList;
        }

    }
}
