﻿namespace FizzBuzz.Business
{
    public interface IFizzBuzzManager
    {
        bool IsNumberDivisible(int number);

        int Order { get; }

        string GetDisplayMessage(bool isTodayWednesday);

    }
}
