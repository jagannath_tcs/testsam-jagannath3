﻿namespace FizzBuzz.Business
{
    using System.Collections.Generic;

    public interface IFizzBuzzManagerLogic
    {
        List<string> GetFizzBuzzList(int inputValue);
    }
}
